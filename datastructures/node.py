class Node:
  def __init__(self, name, id, parent=None):
    self.name          = name
    self.id            = id
    self.parent        = parent
    self.adjacentNodes = [] # this is only used for trees. For graphs we use the adjacency list there. Maybe it is cleaner to only use one of the two.
    self.x             = None
    self.y             = None
    self.depth         = 0
    self.width         = 1
    self.angle_range   = [-1, -1] # used for radial layout
    self.size          = 1 # size of tree, not important for graphs
    self.force         = [0,0]
    self.dummy         = False

  def add_child(self, node, depth=None):
    node.depth = depth
    self.adjacentNodes.append(node)

  def update_size(self):
    if self.adjacentNodes:
      self.size += sum(child.update_size() for child in self.adjacentNodes)

    return self.size