import datastructures.node as nd

class multiLayeredGraph:
    def __init__(self, name) -> None:
        self.name                           = name # e.g. "Dating of the Main Culm"
        self.subgraphs                      = {} # key: name of subgraph, value: subgraph
        self.inter_layer_edges              = {} # edges between different subgraphs
        self.inter_layer_nodes              = {} # invisible nodes

    # adds a node between two existing nodes
    def add_node(self, name, source_name, i, destination):
        # print("add node ", name, " between the source ", source_name, " and the destination ", destination.name)
        self.inter_layer_nodes[name] = nd.Node(name, 66)
        self.inter_layer_edges[name] = [destination]
        self.inter_layer_edges[source_name][i] = self.inter_layer_nodes[name]