import datastructures.node as nd
import loadingFile

class Tree:
  def __init__(self, root=None):
    self.root  = root
    self.nodes = {}

    # If root is given, set up correct values.
    if root:
      self.nodes[root.name] = root
      self.root.x = 0
      self.root.y = 0

  def add_edge(self, parent, child):
    # Check if they are already added to the tree somewhere.
    parent_node = self.nodes.get(parent.name)
    child_node  = self.nodes.get(child.name)

    # If parent doesn't exist yet, add it to the tree.
    if not parent_node:
      parent_node             = nd.Node(parent.name, len(self.nodes))
      parent_node.depth       = 0
      self.nodes[parent.name] = parent_node

      # If root is still empty, set root of the tree.
      if not self.root:
        self.root = parent_node
        self.root.x = 0
        self.root.y = 0

    # If child doesn't exist yet, add it to the tree.
    if not child_node:
      child_node             = nd.Node(child.name, len(self.nodes), parent_node)
      self.nodes[child.name] = child_node

    # Add child to it's parents children.
    parent_node.add_child(child_node, parent_node.depth + 1)

  def is_leaf(self, node):
    return not node.adjacentNodes

  def calculate_depth(self):
    maximumDepth = -1

    for node in self.nodes.values():
      maximumDepth = max(maximumDepth, node.depth)

    return maximumDepth

  def print_tree(self, node=None, level=0):
        if not node:
            node = self.root

        print('  ' * level + node.name + f" ({node.depth})" + f" <{node.x}, {node.y}>")

        for child in node.adjacentNodes:
            self.print_tree(child, level + 1)

if __name__ == "__main__":
  tree = loadingFile.load_tree_from_dataset("customTree", True)
  tree.print_tree()