import datastructures.node as nd

class Graph:
  def __init__(self, name, nodes, is_directed = False):
    self.name                = name
    self.nodes               = {}
    self.adj_list            = {}
    self.adj_edge_label_list = {}
    self.adj_matrix          = []
    self.edge_weights        = []
    self.is_directed         = is_directed

    for i, node in enumerate(nodes):
      node                     = nd.Node(node, i)
      self.nodes[node.name]    = node
      self.adj_list[node.name] = []
    
  def add_node(self, node):
      self.nodes[node.name]    = node
      self.adj_list[node.name] = []

  def remove_edge(self, u, v):
      self.adj_list[u].remove(v)

  def add_edge(self, u, v):
    u = self.nodes.get(u)
    v = self.nodes.get(v)

    self.adj_list[u.name].append(v)
    self.adj_edge_label_list[u.name, v.name] = []

    # If graph is undirected add edge the opposite way as well.
    if not self.is_directed:
      self.adj_list[v.name].append(u)
      self.adj_edge_label_list[v.name, u.name] = []

  def add_label(self, u, v, label):
    u = self.nodes.get(u)
    v = self.nodes.get(v)
    self.adj_edge_label_list[u.name, v.name].append(label)

    # If graph is undirected add attributes to the opposite edge as well.
    if not self.is_directed:
      self.adj_edge_label_list[v.name, u.name].append(label)

  def update_layer_depth(self, layers):
    for i, layer in layers.items():
      for node_name in layer:
        node = self.nodes.get(node_name)
        node.depth = i

  def print_list(self):
    print("Adjacency List for", "directed" if self.is_directed else "undirected", self.name, "graph:")
    for node in self.nodes.values():
      print(node.name, ":", self.adj_list[node.name])

    print("Edges Label List for", "directed" if self.is_directed else "undirected", self.name, "graph:")
    for edge in self.adj_edge_label_list:
      print(edge, ":", self.adj_edge_label_list[edge[0], edge[1]])
