import copy
import random

import datastructures.graph as gr
import datastructures.node  as nd

# Main Functions
def minimumFeedbackSet(graph):
    sources = returnAllSources(graph)
    sinks   = returnAllSinks(graph)

    # No cycles if all verices are only sources and sinks.
    if len(sources) + len(sinks) == len(graph.nodes):
      return graph

    edges         = []
    reversedEdges = []
    nodes         = []

    while len(graph.nodes) > 0:
      sinks = returnAllSinks(graph)

      while len(sinks) > 0:
        node  = sinks.pop(0)
        edges = appendEdgesOfSink(graph, node, edges)
        nodes = removeNode(graph, node, nodes)
        sinks = returnAllSinks(graph)

      nodes   = removeIsolated(graph, nodes)
      sources = returnAllSources(graph)

      while len(sources) > 0:
        node    = sources.pop(0)
        edges   = appendEdgesOfSource(graph, node, edges)
        nodes   = removeNode(graph, node, nodes)
        sources = returnAllSources(graph)

      if len(graph.nodes) > 0:
        node                 = largestDiffNode(graph)
        edges, reversedEdges = appendEdgesOfNode(graph, node, edges, reversedEdges)
        nodes                = removeNode(graph, node, nodes)

    # Contract the DAG
    dag = gr.Graph(graph.name, nodes, True)

    # Add Edges to the Adjacency list.
    edges = list(dict.fromkeys(edges))
    for u, v in edges:
      dag.add_edge(u, v)

    return dag, reversedEdges

def layerAssignment(graph):
    layers = heightMinimization(graph)
    graph.update_layer_depth(layers)
    addDummyNodes(graph, layers)

    return graph, layers

def crossMinimization(graph, layers, k, heuristic):
    for layer in layers.values():
      for j, node_name in enumerate(layer, 1):
        node   = graph.nodes.get(node_name)
        node.x = j

    randomLayoutCount = 0
    best_layers       = layers
    best_crossings    = float('inf')

    while randomLayoutCount < k:
      random.shuffle(layers[1])
      for j, node_name in enumerate(layers[1], 1):
        node   = graph.nodes.get(node_name)
        node.x = j

        new_layers, new_crossings = applyMinimization(graph, layers, k, heuristic)

        if new_crossings < best_crossings:
          best_layers    = new_layers
          best_crossings = new_crossings

        randomLayoutCount += 1

    return best_layers

# Helper Funcctions
def applyMinimization(graph, layers, k, heuristic):
    crossingsCount = float('inf')
    iterationCount = 0
    best_layers    = layers
    direction_up   = True

    while iterationCount < k:
      crossings = 0

      if direction_up:
        for i in range(1, len(layers)):
          layers[i+1] = orderNodes(layers[i], layers[i+1], graph, direction_up, heuristic)
          crossings  += calculateCrossings(graph, layers[i], layers[i+1])
      else:
        for i in range(len(layers), 1, -1):
          layers[i-1] = orderNodes(layers[i], layers[i-1], graph, direction_up, heuristic)
          crossings  += calculateCrossings(graph, layers[i-1], layers[i])

      if crossings < crossingsCount:
        best_layers    = layers
        crossingsCount = crossings
        iterationCount = 0
      else:
        iterationCount += 1

      direction_up = not direction_up

    return best_layers, crossingsCount

def calculateCrossings(graph, layer1, layer2):
    node_order1 = {node: i for i, node in enumerate(layer1)}
    node_order2 = {node: i for i, node in enumerate(layer2)}

    edges = []
    for node1 in layer1:
        for node2 in layer2:
            if graph.nodes.get(node2) in graph.adj_list[node1]:
                edges.append((node1, node2))

    crossings = 0
    for i, (u1, v1) in enumerate(edges):
        for j, (u2, v2) in enumerate(edges):
            if i != j:
                if (node_order1[u1] < node_order1[u2] and
                    node_order2[v1] > node_order2[v2]):
                    crossings += 1
                if (node_order1[u1] > node_order1[u2] and
                    node_order2[v1] < node_order2[v2]):
                    crossings += 1

    return crossings

def orderNodes(fixed_layer, layer, graph, direction_up, heuristic):
    if heuristic == "barycenter":
      new_order = baryCenterHeuristic(fixed_layer, layer, graph, direction_up)
    if heuristic == "median":
      new_order = medianHeuristic(fixed_layer, layer, graph, direction_up)
    else:
      new_order = baryCenterHeuristic(fixed_layer, layer, graph, direction_up)
      new_order = medianHeuristic(fixed_layer, new_order, graph, direction_up)

    return new_order

def baryCenterHeuristic(fixed_layer, layer, graph, direction_up):
  barycenters = {}

  for node in layer:
    degree      = 0
    positionSum = 0

    for neighbourNode in fixed_layer:
      if direction_up:
        if graph.nodes.get(node) in graph.adj_list[neighbourNode]:
          degree      += 1
          positionSum += graph.nodes.get(neighbourNode).x
      else:
        if graph.nodes.get(neighbourNode) in graph.adj_list[node]:
          degree      += 1
          positionSum += graph.nodes.get(neighbourNode).x

    barycenters[node] = positionSum / degree

  new_order = sorted(barycenters.keys(), key=lambda x: barycenters[x])

  for i, node_name in enumerate(new_order, 1):
    node   = graph.nodes.get(node_name)
    node.x = i

  return new_order

def medianHeuristic(fixed_layer, layer, graph, direction_up):
  medians = {}

  for node in layer:
    x_positions = []

    for neighbourNode in fixed_layer:
      if direction_up:
        if graph.nodes.get(node) in graph.adj_list[neighbourNode]:
          x_positions.append(graph.nodes.get(neighbourNode).x)
      else:
        if graph.nodes.get(neighbourNode) in graph.adj_list[node]:
          x_positions.append(graph.nodes.get(neighbourNode).x)
    x_positions.sort()

    if len(x_positions) % 2 == 0:
        medians[node] = (x_positions[len(x_positions)//2] + x_positions[len(x_positions)//2 - 1])/2
    else:
        medians[node] = x_positions[len(x_positions)//2]

  new_order = sorted(medians.keys(), key=lambda x: medians[x])

  for i, node_name in enumerate(new_order, 1):
    node   = graph.nodes.get(node_name)
    node.x = i

  return new_order

def returnAllSources(graph):
    # If a node has outgoing arcs and none of the other nodes have the node as adjacent node, then the node is a source
    sources = []
    for node1 in graph.nodes.values():
        if len(graph.adj_list[node1.name]) > 0:
            sources.append(node1)
            for node2 in graph.nodes.values():
                if node1 in graph.adj_list[node2.name]:
                    sources.remove(node1)
                    break
    return sources

def returnAllSinks(graph):
    # if the node does not have outgoing arcs, then the node is a sink
    return [sink for sink in graph.nodes.values() if len(graph.adj_list[sink.name]) == 0]

def appendEdgesOfSink(graph, node, edges):
    for u, v in graph.adj_list.items():
        if node in v:
            graph.remove_edge(u, node)
            edges.append((u, node.name))

    return edges

def appendEdgesOfSource(graph, node1, edges):
    for node2 in graph.adj_list[node1.name]:
        edges.append((node1.name, node2.name))

    return edges

def appendEdgesOfNode(graph, node1, edges, reversedEdges):
    for node2 in graph.adj_list[node1.name]:
        edges.append((node1.name, node2.name))

    for u, v in graph.adj_list.items():
        if node1 in v:
            graph.remove_edge(u, node1)
            edges.append((node1.name, u))
            reversedEdges.append((u, node1.name))

    return edges, reversedEdges

def removeNode(graph, node, nodes):
    graph.nodes.pop(node.name)
    del graph.adj_list[node.name]
    nodes.append(node.name)

    return nodes

def removeNodes(graph, nodes, i):
    for node in nodes:
      graph.nodes.pop(node.name)
      del graph.adj_list[node.name]

def removeIsolated(graph, nodes):
    in_degrees  = {node: 0 for node in graph.adj_list}
    out_degrees = {node: len(graph.adj_list[node]) for node in graph.adj_list}

    for node in graph.adj_list:
        for neighbor in graph.adj_list[node]:
            in_degrees[neighbor.name] += 1

    isolated_nodes = []
    for node in graph.nodes.values():
        if in_degrees[node.name] == 0 and out_degrees[node.name] == 0:
            isolated_nodes.append(node)

    for node in isolated_nodes:
        graph.nodes.pop(node.name)
        del graph.adj_list[node.name]
        nodes.append(node.name)

    return nodes

def heightMinimization(graph):
    graph    = copy.deepcopy(graph)
    sources  = returnAllSources(graph)
    layers   = {}
    i        = 1

    while len(sources) > 0:
      layers[i] = []
      for source in sources:
        layers[i].append(source.name)

      removeNodes(graph, sources, i)
      sources = returnAllSources(graph)
      i      += 1

    if len(graph.nodes) > 0:
      layers[i] = []
      for node in graph.nodes.values():
        layers[i].append(node.name)

    return layers

def largestDiffNode(graph):
    in_degrees  = {node: 0 for node in graph.adj_list}
    out_degrees = {node: len(graph.adj_list[node]) for node in graph.adj_list}

    for node in graph.adj_list:
        for neighbor in graph.adj_list[node]:
            in_degrees[neighbor.name] += 1

    largest_diff = float('-inf')
    largest_node = None

    for node in graph.adj_list:
        diff = abs(in_degrees[node] - out_degrees[node])
        if diff > largest_diff:
            largest_diff = diff
            largest_node = node

    return graph.nodes.get(largest_node)

def addDummyNodes(graph, layers):
    for i, layer in layers.items():
        for node_name in layer:
            node          = graph.nodes.get(node_name)
            redunantEdges = []

            for adjacentNode in graph.adj_list[node_name]:
                layerDiff = adjacentNode.depth - node.depth
                if layerDiff > 1:
                    count      = 0
                    dummyNodes = []
                    redunantEdges.append(adjacentNode)

                    for j in range(1, layerDiff):
                        dummyNode       = nd.Node(f"{node.name}_dummy_{adjacentNode.name}_{count}", len(graph.nodes) + 1)
                        dummyNode.depth = node.depth + j
                        dummyNode.dummy = True
                        count          += 1

                        layers[i+j].append(dummyNode.name)
                        graph.add_node(dummyNode)
                        dummyNodes.append(dummyNode)

                    graph.add_edge(node.name, dummyNodes[0].name)
                    graph.add_edge(dummyNodes[-1].name, adjacentNode.name)
                    for x in range(1, len(dummyNodes)):
                       graph.add_edge(dummyNodes[x-1].name, dummyNodes[x].name)

            for edge_node in redunantEdges:
              graph.remove_edge(node.name, edge_node)