import math
import numpy                  as np
from scipy.sparse.csgraph     import shortest_path
from sklearn.manifold         import trustworthiness
from sklearn.metrics.pairwise import pairwise_distances

def calculateCrossingsMetrics(graph):
    crossings    = 0
    minAngle = float("inf")

    edges = []
    for node1 in graph.nodes.values():
        for node2 in graph.nodes.values():
            if node2 in graph.adj_list[node1.name]:
                edges.append((node1, node2))

    for i in range(len(edges)):
        node1, node2 = edges[i]
        for j in range(i+1, len(edges)):
            if not node1 in edges[j] and not node2 in edges[j]:
                node3, node4 = edges[j]

                # Cross product method
                if ((node3.y - node1.y) * (node4.x - node3.x) - (node3.x - node1.x) * (node4.y - node3.y)) * ((node3.y - node2.y) * (node4.x - node3.x) - (node3.x - node2.x) * (node4.y - node3.y)) < 0:
                    if ((node1.y - node3.y) * (node2.x - node1.x) - (node1.x - node3.x) * (node2.y - node1.y)) * ((node1.y - node4.y) * (node2.x - node1.x) - (node1.x - node4.x) * (node2.y - node1.y)) < 0:
                        crossings += 1

                        # Calculate Angle
                        vector1  = np.array([node2.x - node1.x, node2.y - node1.y])
                        vector2  = np.array([node4.x - node3.x, node4.y - node3.y])
                        dot      = np.dot(vector1, vector2)
                        angle    = np.degrees(np.arccos(dot / (np.linalg.norm(vector1) *  np.linalg.norm(vector2))))
                        minAngle = min(minAngle, angle)

    return crossings, minAngle

def calculateStressLayout(graph):
    createMatrixFromList(graph)
    stressNom   = 0
    stressDeNom = 0
    distance_matrix = shortest_path(graph.adj_matrix, return_predecessors=False, method='FW', directed=False)

    for i, node1 in enumerate(graph.nodes.values()):
        for j, node2 in enumerate(graph.nodes.values()):
            if i < j:
                d_ij = distance_matrix[i][j]
                stressNom   += ((np.linalg.norm(np.abs(np.array([node1.x, node1.y]) - np.array([node2.x, node2.y]))) - d_ij)**2)
                stressDeNom += (d_ij**2)

    return stressNom / stressDeNom

def createMatrixFromList(graph):
  verticeNumber    = len(graph.adj_list)
  graph.adj_matrix = np.zeros((verticeNumber, verticeNumber))

  for i, vertex in enumerate(graph.adj_list):
    for neighbor in graph.adj_list[vertex]:
        graph.adj_matrix[i][neighbor.id] = 1

def calculateProjectionMetrics(graph, layout):
    createMatrixFromList(graph)
    distance_matrix_graph      = pairwise_distances(graph.adj_matrix, metric="euclidean")
    distance_matrix_projection = pairwise_distances(layout, metric="euclidean")

    trust      = trustworthiness(distance_matrix_graph, distance_matrix_projection)

    return trust

def calculateStressProjection(graph, layout):
    createMatrixFromList(graph)
    stressNom   = 0
    stressDeNom = 0
    distance_matrix = shortest_path(graph.adj_matrix, return_predecessors=False, method='FW', directed=False)

    for i, node1 in enumerate(graph.nodes.values()):
        for j, node2 in enumerate(graph.nodes.values()):
            if i < j:
                d_ij_nD  = distance_matrix[i][j]
                d_ij_Pro = math.dist((node1.x, node1.y), (node2.x, node2.y))
                stressNom   += d_ij_Pro - (d_ij_nD ** 2)
                stressDeNom += (d_ij_nD ** 2)

    return stressNom / stressDeNom