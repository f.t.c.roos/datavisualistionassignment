import networkx                         as nx
import pydotplus

import datastructures.graph             as gr
import datastructures.tree              as tr
import datastructures.multiLayeredGraph as mlg
from utils import get_path

def load_tree_from_dataset(dataset, printTree=False):
  dotFile = nx.nx_pydot.read_dot(get_path(dataset))

  # Some Dot Files add this random node at the end, this small snippet fixes it.
  if dotFile.has_node("\\n"):
    dotFile.remove_node("\\n")

  tree = tr.Tree()

  for parent, child in dotFile.edges():
      tree.add_edge(parent, child)

  if printTree:
    tree.print_tree()

  return tree

def load_graph_from_dataset(dataset, printGraph=False):
  dotFile = nx.nx_pydot.read_dot(get_path(dataset))

  # Some Dot Files add this random node at the end, this small snippet fixes it.
  if dotFile.has_node("\\n"):
    dotFile.remove_node("\\n")

  directed = True if dotFile.is_directed() else False
  graph    = gr.Graph(dataset, dotFile.nodes(), directed)

  # Add Edges to the Adjacency list, and store their attributes in a seperate list.
  for u, v in dotFile.edges():
    graph.add_edge(u, v)

    for labels in dotFile.get_edge_data(u, v).values():
      for label in labels.items():
        graph.add_label(u, v, label)

  if printGraph:
    graph.print_list()

  return graph

def load_multilayer_graph_from_dataset(dataset, nameSubgraph1, nameSubgraph2, printGraph=False):
  input = pydotplus.graphviz.graph_from_dot_file(get_path(dataset))
  multi_layered_graph = mlg.multiLayeredGraph(input.get_name())
  directed = True # if dotFile.is_directed() else False

  nodes = [node.get_name() for node in input.get_subgraph(nameSubgraph1)[0].get_nodes()]
  graph = gr.Graph(nameSubgraph1, nodes, directed)
  multi_layered_graph.subgraphs[nameSubgraph1] = graph

  nodes = [node.get_name() for node in input.get_subgraph(nameSubgraph2)[0].get_nodes()]
  graph = gr.Graph(nameSubgraph2, nodes, directed)
  multi_layered_graph.subgraphs[nameSubgraph2] = graph

  for edge in input.get_edges():
    if edge.get_source() in multi_layered_graph.subgraphs[nameSubgraph1].nodes:
      if edge.get_destination() in multi_layered_graph.subgraphs[nameSubgraph1].nodes: # intra layer edge
        multi_layered_graph.subgraphs[nameSubgraph1].add_edge(edge.get_source(), edge.get_destination())
      else: # inter layer edge
        destination = multi_layered_graph.subgraphs[nameSubgraph2].nodes.get(edge.get_destination())
        if edge.get_source() in multi_layered_graph.inter_layer_edges:
          multi_layered_graph.inter_layer_edges[edge.get_source()].append(destination)
        else:
          multi_layered_graph.inter_layer_edges[edge.get_source()] = [destination]
    else: # edge.get_source() is in multi_layered_graph.subgraphs[nameSubgraph2].nodes:
      if edge.get_destination() in multi_layered_graph.subgraphs[nameSubgraph2].nodes: # intra layer edge
        multi_layered_graph.subgraphs[nameSubgraph2].add_edge(edge.get_source(), edge.get_destination())
      else: # inter layer edge
        destination = multi_layered_graph.subgraphs[nameSubgraph1].nodes.get(edge.get_destination())
        if edge.get_source() in multi_layered_graph.inter_layer_edges:
          multi_layered_graph.inter_layer_edges[edge.get_source()].append(destination)
        else:
          multi_layered_graph.inter_layer_edges[edge.get_source()] = [destination]

  if printGraph:
    print(multi_layered_graph.inter_layer_edges)
    for subgraph in multi_layered_graph.subgraphs:
      print(multi_layered_graph.subgraphs[subgraph].adj_list)

  return multi_layered_graph

if __name__ == "__main__":
  nameSubgraph1 = "ZonderAanhalingstekens"
  nameSubgraph2 = "\"MetAanhalingstekens\""
  multiGraph = load_multilayer_graph_from_dataset("customMultiLayeredGraph", nameSubgraph1, nameSubgraph2, False) # ("argumentation ")