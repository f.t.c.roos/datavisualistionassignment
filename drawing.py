from functools            import partial
import matplotlib.pyplot  as plt
from matplotlib.patches   import Circle, ConnectionPatch, Rectangle
from matplotlib.animation import FuncAnimation
import numpy              as np
import math
import random
import sys
import re
import copy

import loadingFile          as lf
import forceDirectedHelpers as fdh
import layeredHelpers       as lh
import projectionHelpers    as ph
import metricHelpers        as mh

def drawProjectionGraph(graph, projection):
    ax = drawSettings()
    ph.createWeightMatrixFromList(graph)
    layout = ph.createSimilarityMatrix(graph, projection)

    plt.scatter(layout[:,0], layout[:,1], color="darkred", zorder=10)

    for node in graph.nodes.values():
        for adjacentNode in graph.adj_list[node.name]:
            ax.add_patch(ConnectionPatch([node.x, node.y], [adjacentNode.x, adjacentNode.y], 'data', arrowstyle="-", linewidth=0.15, facecolor="black"))
    plt.show()

def drawMultiLayeredGraph(multi_layered_graph, nameSubgraph1, nameSubgraph2):
    graph1 = multi_layered_graph.subgraphs[nameSubgraph1]
    graph2 = multi_layered_graph.subgraphs[nameSubgraph2]

    FruchtermanReingoldSpringEmbedder(graph1)
    fdh.linearMapping(graph1, t_min_x = 0, t_max_x = 1, t_min_y = 0, t_max_y = 1)

    FruchtermanReingoldSpringEmbedder(graph2)
    fdh.linearMapping(graph2, t_min_x = 0.85, t_max_x = 1.85, t_min_y = 0.85, t_max_y = 1.85)

    ax = drawSettings()
    plt.title(f"Multi layered drawing for the Argumentation Network")

    arrowstyle = "-|>" if graph1.is_directed else "-" # Make the edges directed or undirected.
    size = len(graph1.nodes) + len(graph2.nodes)
    radius = 0.25 / (size + 4)
    lineWidth = 0.5 - (size / 198) * 0.45

    # drawing the intra layer edges
    for graph in [graph1, graph2]:
        for node in graph.nodes.values():
                ax.add_patch(Circle([node.x, node.y], radius=radius, fill=True, zorder=10, color="darkred"))
                for adjacentNode in graph.adj_list[node.name]:
                    ax.add_patch(ConnectionPatch([adjacentNode.x, adjacentNode.y], [node.x, node.y], 'data', arrowstyle=arrowstyle, linewidth=lineWidth, color="black"))

    # drawing the inter layer edges
    for inter_layer_edge in multi_layered_graph.inter_layer_edges:
        if inter_layer_edge in graph1.nodes:
            source = graph1.nodes[inter_layer_edge]
        else:
            source = graph2.nodes[inter_layer_edge]

        for destination in multi_layered_graph.inter_layer_edges[inter_layer_edge]:
            ax.add_patch(ConnectionPatch([source.x, source.y], [destination.x, destination.y], 'data', arrowstyle=arrowstyle, linewidth=lineWidth, color="red"))

    plt.show()
    plt.close()

def drawEdgeBundlingMultiLayeredGraph(mlg, nameSubgraph1, nameSubgraph2):
    # start parameters, mostly decided via experimenting by checking the end result and the intermediate values
    cycles = 3# 6  # amount of cycles
    n = 1       # initial number of subdivisions
    l = 50      # initial number of iterations
    s = 0.04    # step size
    # end parameters

    graph1 = mlg.subgraphs[nameSubgraph1]
    graph2 = mlg.subgraphs[nameSubgraph2]

    c = 1       # cycle counter
    t = 1       # iteration counter

    while c <= cycles:
        inter_layer_edges = copy.deepcopy(mlg.inter_layer_edges)
        for source_name in inter_layer_edges:
            if source_name in graph1.nodes:
                source = graph1.nodes[source_name]
            elif source_name in graph2.nodes:
                source = graph2.nodes[source_name]
            else:
                source = mlg.inter_layer_nodes[source_name]

            for i, destination in enumerate(mlg.inter_layer_edges[source_name]):
                # subdivide edge P by n points: P1 ... Pn
                for j in range(n):
                    name = source.name + "i" + str(i) + "c" + str(c) + "j" + str(j)
                    mlg.add_node(name, source.name, i, destination)
                    mlg.inter_layer_nodes[name].x = source.x + ((j+1)/(n+1)) * (destination.x - source.x)
                    mlg.inter_layer_nodes[name].y = source.y + ((j+1)/(n+1)) * (destination.y - source.y)

        # foreach P ∈ E do
            # foreach 0 < i < n do
                # F_Pi = k_P · (||P_i−1 − P_i )|| + ||P_i − P_i+1||)
                    # foreach Q != P ∈ E do
                        # foreach 0 < j < n do
                            # F_Pi = F_Pi + C_e(P,Q) / ||P_i − Q_j||

        # foreach p ∈ B do
            # p = p + s · F_p

        # t = t + 1

        # if t == I then
            # t = 1
            # c = c + 1
            # n = 2n
            # s = s/2
            # decrease(I)

        c += 1

    ax = drawSettings()
    plt.title(f"Multi layered drawing for the {mlg.name} graph with Edge bundling")

    arrowstyle = "-|>" if graph1.is_directed else "-" # Make the edges directed or undirected.
    size = len(graph1.nodes) + len(graph2.nodes)
    radius = 0.25 / (size + 4)
    lineWidth = 0.5 - (size / 198) * 0.45

    # drawing the intra layer edges
    for graph in [graph1, graph2]:
        for node in graph.nodes.values():
            ax.add_patch(Circle([node.x, node.y], radius=radius, fill=True, zorder=10, color="darkred"))
            for adjacentNode in graph.adj_list[node.name]:
                ax.add_patch(ConnectionPatch([adjacentNode.x, adjacentNode.y], [node.x, node.y], 'data', arrowstyle=arrowstyle, linewidth=lineWidth, color="black"))

    # drawing the inter layer edges
    for inter_layer_edge in mlg.inter_layer_edges:
        if inter_layer_edge in graph1.nodes:
            source = graph1.nodes[inter_layer_edge]
        elif inter_layer_edge in graph2.nodes:
            source = graph2.nodes[inter_layer_edge]
        else:
            source = mlg.inter_layer_nodes[inter_layer_edge]

        for destination in mlg.inter_layer_edges[inter_layer_edge]:
            ax.add_patch(ConnectionPatch([source.x, source.y], [destination.x, destination.y], 'data', arrowstyle=arrowstyle, linewidth=lineWidth, color="red"))

    # drawing the 'invisible' inter layer nodes
    for inter_layer_node in mlg.inter_layer_nodes:
        node = mlg.inter_layer_nodes[inter_layer_node]
        ax.add_patch(Circle([node.x, node.y], radius=radius/2, fill=True, zorder=10, color="red"))

    plt.show()

def drawDirectedGraphLayered(graph, heuristic, showDummy):
    ax = drawSettings()

    # Based these number on node length for now.
    line_wdith = max(0.03, 10 / len(graph.nodes))

    DAG, reversedEdges = lh.minimumFeedbackSet(graph)
    DAG, layers        = lh.layerAssignment(DAG)
    best_layers        = lh.crossMinimization(DAG, layers, 5, heuristic)

    dummyRadius = 0.1 if showDummy else 0
    rad = 0.5 if graph.name == "smallDir" else 1

    for i, layer in best_layers.items():
      x = 0 - (len(layer))

      for node_name in layer:
        node   = DAG.nodes.get(node_name)
        node.y = i * 5
        node.x = x
        x = x + 2

        if not node.dummy:
          ax.add_patch(Circle([node.x, node.y], radius=rad, fill=True, color="darkred"))
        else:
          ax.add_patch(Circle([node.x, node.y], radius=dummyRadius, fill=False, color="darkblue"))

    for node in DAG.nodes.values():
        for adjacentNode in DAG.adj_list[node.name]:
            # Draw a line from node to adjacent node
            if node.dummy and adjacentNode.dummy:
              if (re.split('_+', adjacentNode.name)[2], re.split('_+', node.name)[0]) in reversedEdges and graph.name == "smallDir":
                edgeColor = "blue"
              else:
                edgeColor = "black"
              nodeX, nodeY, adjacentNodeX, adjacentNodeY = calculateConnectionDistance(node, adjacentNode, dummyRadius, dummyRadius)
              arrowstyle ='-'
            if not node.dummy and adjacentNode.dummy:
              if (re.split('_+', adjacentNode.name)[2], node.name) in reversedEdges and graph.name == "smallDir":
                adjacentNodeX, adjacentNodeY, nodeX, nodeY = calculateConnectionDistance(node, adjacentNode, rad, dummyRadius)
                edgeColor  = "blue"
                arrowstyle ='-|>'
              else:
                nodeX, nodeY, adjacentNodeX, adjacentNodeY = calculateConnectionDistance(node, adjacentNode, rad, dummyRadius)
                edgeColor  = "black"
                arrowstyle ='-'
            if node.dummy and not adjacentNode.dummy:
              if (adjacentNode.name, re.split('_+', node.name)[0]) in reversedEdges and graph.name == "smallDir":
                adjacentNodeX, adjacentNodeY, nodeX, nodeY = calculateConnectionDistance(node, adjacentNode, dummyRadius, rad)
                edgeColor  = "blue"
                arrowstyle ='-'
              else:
                nodeX, nodeY, adjacentNodeX, adjacentNodeY = calculateConnectionDistance(node, adjacentNode, dummyRadius, rad)
                edgeColor  = "black"
                arrowstyle ='-|>'
            if not node.dummy and not adjacentNode.dummy:
              if (adjacentNode.name, node.name) in reversedEdges and graph.name == "smallDir":
                adjacentNodeX, adjacentNodeY, nodeX, nodeY = calculateConnectionDistance(node, adjacentNode, rad, rad)
                edgeColor  = "blue"
              else:
                nodeX, nodeY, adjacentNodeX, adjacentNodeY = calculateConnectionDistance(node, adjacentNode, rad, rad)
                edgeColor  = "black"
              arrowstyle='-|>'

            ax.add_patch(ConnectionPatch([nodeX, nodeY], [adjacentNodeX, adjacentNodeY], 'data', arrowstyle=arrowstyle, linewidth=min(1, line_wdith), zorder=10, color=edgeColor))

    plt.show()

def drawGraph(graph, title):
    ax = drawSettings()
    plt.title(title)

    radius = 0.25 / (len(graph.nodes) + 4)
    lineWidth = 0.3 - ((len(graph.nodes) / 198) * 0.25)
    for node in graph.nodes.values():
        ax.add_patch(Circle([node.x, node.y], radius=radius, fill=True, zorder=10, color="darkred"))
        for adjacentNode in graph.adj_list[node.name]:
            # arrowstyle="-" is hard-coded, because this visualisation is only for undirected graphs
            ax.add_patch(ConnectionPatch([adjacentNode.x, adjacentNode.y], [node.x, node.y], 'data', arrowstyle="-", linewidth=lineWidth))

    plt.show()

def FruchtermanReingoldPlacementAndDrawing(graph):
    FruchtermanReingoldSpringEmbedder(graph)
    drawGraph(graph, f"Fruchterman & Reingold Spring-Embedder for the {graph.name} graph")

def FruchtermanReingoldSpringEmbedder(graph):
    # start parameters, mostly decided via experimenting by checking the end result and the intermediate values
    c = 1
    k = 50
    epsilon = 2
    delta = min(0.1, 1 / len(graph.nodes)) # ideally 0.006 for jazz
    # end parameters

    l = c * (math.sqrt(1 / len(graph.nodes)))
    # Initial circular placement. A fixed circular placement helps to compare different runs on 1 graph input
    # The radius of the circle is the ideal edge length, otherwise the forces explode
    for node in graph.nodes.values():
        node.x = l * ((np.cos([2 * np.pi * float(node.id - 1) / len(graph.nodes)])[0]))
        node.y = l * ((np.sin([2 * np.pi * float(node.id - 1) / len(graph.nodes)])[0]))

    l2 = math.pow(l, 2)
    t = 0
    max_force = epsilon + 1 # dummy value to start the while loop
    while t < k and max_force > epsilon:
        max_force = 0

        for node_v in graph.nodes.values():
            node_v.force = [0,0]

            # repulsive force between all node pairs u and v
            for node_u in graph.nodes.values():
                if node_u != node_v:
                    force = l2 / (fdh.euclideanDistance(node_u, node_v) + sys.float_info.min)
                    vector = fdh.unitVector(node_u, node_v)
                    repulsiveForce = [force * vector[0], force * vector[1]]
                    node_v.force = [node_v.force[0] + repulsiveForce[0], node_v.force[1] + repulsiveForce[1]]

            # attractive force between two adjacent nodes u and v
            for node_u in graph.adj_list[node_v.name]:
                force = math.pow(fdh.euclideanDistance(node_v, node_u), 2) / l
                vector = fdh.unitVector(node_v, node_u)
                attractiveForce = [force * vector[0], force * vector[1]]
                node_v.force = [node_v.force[0] + attractiveForce[0], node_v.force[1] + attractiveForce[1]]

            vectorLength = math.sqrt(math.pow(node_v.force[0], 2) + math.pow(node_v.force[1], 2))
            if vectorLength > max_force:
                max_force = vectorLength

        for node in graph.nodes.values():
            node.x = node.x + delta * node.force[0]
            node.y = node.y + delta * node.force[1]

        delta = 0.99 * delta
        t = t + 1

def EadesPlacementAndDrawing(graph):
    EadesSpringEmbedder(graph)
    drawGraph(graph, f"Eades Spring-Embedder for the {graph.name} graph")

def EadesSpringEmbedder(graph):
    # Start parameters, decided via experimenting
    c_spring = 0.01 # spring constant
    l = 0.2 # ideal spring length for edges
    c_rep = 0.001 # repulsion constant
    k = 100
    epsilon = 0.1
    delta = 0.4 - ((len(graph.nodes) / 198) * 0.2)
    # End parameters

    # Initial circular placement: a fixed circular placement helps to compare different runs on 1 graph input
    # The radius of the circle should maybe be the ideal edge length
    for node in graph.nodes.values():
        node.x = 1 * ((np.cos([2 * np.pi * float(node.id - 1) / len(graph.nodes)])[0]))
        node.y = 1 * ((np.sin([2 * np.pi * float(node.id - 1) / len(graph.nodes)])[0]))

    t = 0
    max_force = epsilon + 1 # dummy value to start the while loop
    while t < k and max_force > epsilon:
        max_force = 0

        for node_v in graph.nodes.values():
            # We want to calculate the som of the forces from v, so we init with 0
            node_v.force = [0,0]

            # from every v, we go to ever node u
            for node_u in graph.nodes.values():
                if node_v in graph.adj_list[node_u.name]:
                    # attractive force between adjacent vertices u and v
                    force = c_spring * math.log(fdh.euclideanDistance(node_u, node_v) / l)
                    vector = fdh.unitVector(node_v, node_u)
                    springForce = [force * vector[0], force * vector[1]]
                    node_v.force = [node_v.force[0] + springForce[0], node_v.force[1] + springForce[1]]
                elif node_u != node_v: # node_v not in graph.adj_list[node_u.name]
                    # repulsive force between two non-adjacent nodes u and v
                    force = c_rep / math.pow(fdh.euclideanDistance(node_v, node_u), 2)
                    vector = fdh.unitVector(node_u, node_v)
                    repulsiveForce = [force * vector[0], force * vector[1]]
                    node_v.force = [node_v.force[0] + repulsiveForce[0], node_v.force[1] + repulsiveForce[1]]

            vectorLength = math.sqrt(math.pow(node_v.force[0], 2) + math.pow(node_v.force[1], 2))
            if vectorLength > max_force:
                max_force = vectorLength

        for node in graph.nodes.values():
            node.x = node.x + delta * node.force[0]
            node.y = node.y + delta * node.force[1]

        delta = 0.99 * delta
        t = t + 1

def calculate_positions_layered(tree, ax, node, circle_rad=0.4, y_size=1, x=0, y=0):
  # Makes sure the root node is drawn.
  if node == tree.root:
    ax.add_patch(Circle((node.x, node.y), radius=circle_rad, fill=True, zorder=10, color="darkred"))

  # Goes over all child nodes recursivly and draws them.
  if not tree.is_leaf(node):
    # Start position of the first child based on the maximum size of the subtree of the node.
    start_x = x - (node.size - 2) / 2

    for i, child in enumerate(node.adjacentNodes):
        # Calculate the size a child needs based on the maximum with of the subtree of the child.
        child_displacement = ((child.size - 1) / 2) if child.size > 1 else 0

        # Set the child positions and the start position of the next child.
        child.x  = start_x + child_displacement if len(node.adjacentNodes) > 1 else x
        child.y  = y - y_size
        start_x += child.size

        # Draw the child nodes and the edges from the parent node.
        ax.add_patch(Circle((node.x, node.y), radius=circle_rad, fill=True, zorder=10, color="darkred"))
        # ax.plot([node.x, child.x], [node.y, child.y], 'k-', linewidth=0.3)

        ax.add_patch(ConnectionPatch([node.x, node.y], [child.x, child.y], 'data', arrowstyle="-", linewidth=0.3, facecolor="black"))

        # Recursivly go down the tree.
        calculate_positions_layered(tree, ax, child, circle_rad=circle_rad, y_size=y_size, x=child.x, y=child.y)

  # Makes sure the leaf nodes are drawn.
  else:
    ax.add_patch(Circle((node.x, node.y), radius=circle_rad, fill=True, zorder=10, color="darkred"))

def drawTreeLayered(tree, y_size, circle_rad, bonus=False, non_edges=[]):
    ax = drawSettings()

    # Recursivly go over each node to calculate the position and draw the nodes and edges during the calculation.
    calculate_positions_layered(tree, ax, tree.root, circle_rad, y_size)

    if bonus:
      for pair in non_edges:
        node  = tree.nodes.get(pair[0].name)
        child = tree.nodes.get(pair[1].name)

        ax.add_patch(ConnectionPatch([node.x, node.y], [child.x, child.y], 'data', arrowstyle="-", linewidth=0.3, color="blue"))

    # plt.savefig("layered.png", bbox_inches="tight", dpi=600)
    plt.show()

def drawTreeForRadialLayout(tree, bonus=False, non_edges=[]):
    ax = drawSettings()

    depth = tree.calculate_depth()

    nodesRadius = 0.001
    # nodesRadius = min(1 / len(tree.nodes), 0.1)
    # radiusSteps = 4
    radiusSteps = (0.5 - nodesRadius) / (depth - 1)

    # Draw the root node
    tree.root.angle_range = [0, 2 * math.pi]
    ax.add_patch(Circle((tree.root.x, tree.root.y), radius=nodesRadius, fill=True, zorder=10, color="darkred"))

    for i in range(1, depth + 1):
        ax.add_patch(Circle((tree.root.x, tree.root.y), radius = radiusSteps * i, fill = False, linewidth=0.1))

    queue = [tree.root]

    while len(queue) > 0:
        node = queue.pop(0)
        # We keep track of the angle that is already covered by the previous children of this parent
        startOfAngleDomain = node.angle_range[0]
        # This angle is used the calculation to avoid crossings and can be calculated only once by extracting it from the while loop
        angle2 = math.acos((radiusSteps * (node.depth + 1)) / (radiusSteps * (node.depth + 2)))
        # The radius of an adjacent node can be computed with the depth of the current node + 1
        radius = radiusSteps * (node.depth + 1)

        for adjacentNode in node.adjacentNodes:
            # The angle of a node takes the appropriate part of the domain based on the size of the subtree
            angle1 = (adjacentNode.size / (node.size - 1)) * (node.angle_range[1] - node.angle_range[0])
            # the angle from the root node to the current node's child node
            nodeAngle = startOfAngleDomain + (angle1 / 2) # the angle from the root to the node

            adjacentNode.x = tree.root.x + (radius * math.cos(nodeAngle))
            adjacentNode.y = tree.root.y + (radius * math.sin(nodeAngle))

            ax.add_patch(Circle([adjacentNode.x, adjacentNode.y], radius=nodesRadius, fill=True, zorder=10, color="darkred"))
            ax.add_patch(ConnectionPatch([node.x, node.y], [adjacentNode.x, adjacentNode.y], 'data',
                                         arrowstyle="-", linewidth=0.3, facecolor="black"))

            # the domain/range of the angle should be bounded to avoid crossings
            # if the tree size based angle is too big
            if angle1 <= (2 * angle2):
                adjacentNode.angle_range[0] = startOfAngleDomain
                adjacentNode.angle_range[1] = startOfAngleDomain + angle1
                startOfAngleDomain += angle1
            else: # This avoid crossings if the
                adjacentNode.angle_range[0] = nodeAngle - angle2
                adjacentNode.angle_range[1] = nodeAngle + angle2
                # this is a design choice
                startOfAngleDomain += angle1 # startOfAngleDomain = adjacentNode.angle_range[1]

        queue.extend(node.adjacentNodes)

    if bonus:
      for pair in non_edges:
        node  = tree.nodes.get(pair[0].name)
        child = tree.nodes.get(pair[1].name)

        ax.add_patch(ConnectionPatch([node.x, node.y], [child.x, child.y], 'data', arrowstyle="-", linewidth=0.3, color="blue"))

    plt.show()

def drawGraphOfSingleCircle(graph, r=10):
    ax = drawSettings()
    plt.title(f"Single circle drawing for the {graph.name} graph")
    # Based these number on node length for now.
    circle_rad = min(0.5, 20 / len(graph.nodes))
    line_wdith = max(0.03, 20 / len(graph.nodes))

    # Make the edges directed or undirected.
    arrowstyle = "-|>" if graph.is_directed else "-"

    for node in graph.nodes.values():
        node.x = r * ((np.cos([2 * np.pi * float(node.id - 1) / len(graph.nodes)])[0]))
        node.y = r * ((np.sin([2 * np.pi * float(node.id - 1) / len(graph.nodes)])[0]))

        ax.add_patch(Circle([node.x, node.y], radius=circle_rad, fill=True, color="darkred"))

    for node in graph.nodes.values():
        for adjacentNode in graph.adj_list[node.name]:
            # Draw a line from node to adjacent node
            nodeX, nodeY, adjacentNodeX, adjacentNodeY = calculateConnectionDistance(node, adjacentNode, circle_rad, circle_rad)
            ax.add_patch(ConnectionPatch([nodeX, nodeY], [adjacentNodeX, adjacentNodeY], 'data',
                                         arrowstyle=arrowstyle, linewidth=min(1, line_wdith), zorder=10, facecolor="black"))

    plt.show()

def exampleDrawing():
    plt.rcParams['figure.figsize'] = [8, 8]
    fig, ax = plt.subplots()
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.0])
    plt.title(f"This is a test drawing")

    # Generate the circle's position somehow
    pos_circle = np.random.rand(2)

    ax.add_patch(Circle(pos_circle, radius=0.1, fill=False))

    # Draw a line from circle to somewhere else
    ax.add_patch(ConnectionPatch(pos_circle, np.random.rand(2),'data'))

    # Generate the rectangle's position somehow
    pos_rectangle = np.random.rand(2)
    ax.add_patch(Rectangle(pos_rectangle, 0.1, 0.1, fill=False))

    # dummy parameter
    randomBool = random.choice([True, False])

    def add_figure(i, fillRectangle):
        #ax.clear()
        # Generate the rectangle's position somehow
        pos_rectangle = np.random.rand(2)
        ax.add_patch(Rectangle(pos_rectangle, 0.1, 0.1, fill=fillRectangle))

        ax.plot()

    anim = FuncAnimation(fig, partial(add_figure, fillRectangle=randomBool), interval=500)

    plt.show()

def drawSettings():
    plt.rcParams['figure.figsize'] = [8, 8]
    _, ax = plt.subplots()
    ax.set_aspect('equal')
    ax.axis('off')
    ax.autoscale()
    ax.margins(0.1)

    return ax

def calculateConnectionDistance(node, adjacentNode, rad_node, rad_adjacentNode):
    dx       = adjacentNode.x - node.x
    dy       = adjacentNode.y - node.y
    distance = math.sqrt(dx*dx + dy*dy)

    ux = dx / distance
    uy = dy / distance

    r = rad_node
    nodex = node.x + r * ux
    nodey = node.y + r * uy

    r = rad_adjacentNode
    anodex = adjacentNode.x - r * ux
    anodey = adjacentNode.y - r * uy

    return nodex, nodey, anodex, anodey

def calculateConnectionDistanceBrezier(node, adjacentNode, rad_node, rad_adjacentNode):
    dx       = adjacentNode.x - node.x
    dy       = adjacentNode.y - node.y
    distance = math.sqrt(dx*dx + dy*dy)

    offset_factor = 0.5

    ux = dx / distance
    uy = dy / distance

    # Calculate a point along the line connecting the nodes
    px = node.x + distance/2 * ux
    py = node.y + distance/2 * uy

    # Calculate a unit vector perpendicular to the line
    nx, ny = -uy, ux

    # Calculate the offset for the control point
    offset = distance * offset_factor

    # Calculate the control point
    cpx = px + offset * nx
    cpy = py + offset * ny

    r = rad_node
    nodex = node.x + r * ux
    nodey = node.y + r * uy

    r = rad_adjacentNode
    anodex = adjacentNode.x - r * ux
    anodey = adjacentNode.y - r * uy

    return nodex, nodey, cpx, cpy, anodex, anodey

if __name__ == "__main__":
    exampleDrawing()