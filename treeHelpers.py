import random
import queue
import datastructures.tree as tree

def bfs(graph, index = None):
    if index == None:
        index = random.randint(0, len(graph.nodes) - 1)
        print("Random picked index is: ", index)

    nodes      = list(graph.nodes.values())
    bfs_tree   = tree.Tree(nodes[index])
    q          = queue.Queue()
    visited    = set()
    start_node = nodes[index]

    q.put(start_node)
    visited.add(start_node.name)

    while not q.empty():
      current_node = q.get()

      for child in graph.adj_list[current_node.name]:
        if child.name not in visited:
          bfs_tree.add_edge(current_node, child)
          visited.add(child.name)
          q.put(child)

    return bfs_tree

def dfs(graph, index = None):
    if index == None:
        index = random.randint(0, len(graph.nodes) - 1)
        print("Random picked index is: ", index)

    nodes      = list(graph.nodes.values())
    dfs_tree   = tree.Tree(nodes[index])
    visited    = set()
    start_node = nodes[index]

    _dfs(graph, dfs_tree, start_node, visited)

    return dfs_tree

def _dfs(graph, dfs_tree, current_node, visited):
    visited.add(current_node.name)

    for child in graph.adj_list[current_node.name]:
      if child.name not in visited:
        dfs_tree.add_edge(current_node, child)
        _dfs(graph, dfs_tree, child, visited)