import random
import queue
import datastructures.tree as tree

def bfs_non_edges(graph, index=None):
    if index is None:
        index = random.randint(0, len(graph.nodes) - 1)
        print("Randomly picked index is:", index)

    nodes = list(graph.nodes.values())
    bfs_tree = tree.Tree(nodes[index])
    q = queue.Queue()
    visited = set()
    start_node = nodes[index]
    non_tree_edges = []
    added_edges    = []

    q.put(start_node)
    visited.add(start_node.name)

    while not q.empty():
        current_node = q.get()

        for child in graph.adj_list[current_node.name]:
            if child.name not in visited:
                bfs_tree.add_edge(current_node, child)
                added_edges.append((current_node, child))
                visited.add(child.name)
                q.put(child)
            else:
                if (child, current_node) not in added_edges:
                  non_tree_edges.append((current_node, child))

    return bfs_tree, non_tree_edges

def dfs_non_edges(graph, index=None):
    if index is None:
        index = random.randint(0, len(graph.nodes) - 1)
        print("Randomly picked index is:", index)

    nodes = list(graph.nodes.values())
    dfs_tree = tree.Tree(nodes[index])
    visited = set()
    start_node = nodes[index]
    non_tree_edges = []
    added_edges    = []

    non_tree_edges = _dfs_non_edges(graph, dfs_tree, start_node, visited, non_tree_edges, added_edges)

    return dfs_tree, non_tree_edges

def _dfs_non_edges(graph, dfs_tree, current_node, visited, non_tree_edges, added_edges):
    visited.add(current_node.name)

    for child in graph.adj_list[current_node.name]:
        if child.name not in visited:
            dfs_tree.add_edge(current_node, child)
            added_edges.append((current_node, child))
            _dfs_non_edges(graph, dfs_tree, child, visited, non_tree_edges, added_edges)
        else:
            if (child, current_node) not in added_edges:
                non_tree_edges.append((current_node, child))

    return non_tree_edges