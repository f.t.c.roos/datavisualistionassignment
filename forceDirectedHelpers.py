import math

# debug helper function
def printLocationOfEveryNode(graph):
    for node in graph.nodes.values():
        print("Node ", node.name, ": at location: x = ", node.x, ", y = ", node.y)
    print("--------------------------------------")

# push coordinates of nodes into a certain domain
def linearMapping(graph, t_min_x = 0.1, t_max_x = 0.9, t_min_y = 0.1, t_max_y = 0.9):
    v_x_min = min(node.x for node in graph.nodes.values())
    v_x_max = max(node.x for node in graph.nodes.values())
    v_y_min = min(node.y for node in graph.nodes.values())
    v_y_max = max(node.y for node in graph.nodes.values())

    for node in graph.nodes.values():
        node.x = (((node.x - v_x_min) / (v_x_max - v_x_min)) * (t_max_x - t_min_x)) + t_min_x
        node.y = ((node.y - v_y_min) / (v_y_max - v_y_min)) * (t_max_y - t_min_y) + t_min_y

def euclideanDistance(node_u, node_v):
    return math.sqrt(math.pow(node_u.x - node_v.x, 2) + math.pow(node_u.y - node_v.y, 2))

# unit vector pointing from u to v for 2 dimensions
def unitVector(node_u, node_v):
    lengthVectorUV = euclideanDistance(node_u, node_v)

    # Quick fix for same node placement
    if lengthVectorUV == 0:
      lengthVectorUV = 0.000000000000001
    return [(node_v.x - node_u.x) / lengthVectorUV, (node_v.y - node_u.y) / lengthVectorUV]