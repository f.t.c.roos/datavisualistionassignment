# Map all the names of the datasets to their relative path.
dataset_devname_to_path = {
  "lesMis":                   "datasets/LesMiserables.dot",
  "rome":                     "datasets/rome.dot",
  "jazz":                     "datasets/JazzNetwork.dot",
  "smallDir":                 "datasets/noname.dot",
  "proLeague":                "datasets/LeagueNetwork.dot",
  "argumentation":            "datasets/devonshiredebate_withclusters.dot",
  "argumentationSmaller":     "datasets/devonshiredebate_withonlytwoclusters.dot",
  "polBlog":                  "datasets/polblogs.dot",
  "customGraph":              "datasets/customGraph.dot",
  "customDir":                "datasets/customGraphDir.dot",
  "customTree":               "datasets/customTree.dot",
  "customTree2":              "datasets/customTree2.dot",
  "customGraphFD":            "datasets/customGraphForceDirected.dot",
  "customMultiLayeredGraph":  "datasets/customMultiLayeredGraph.dot"
}

# Map all the real names of the datasets to their developer names.
dataset_realname_to_devname = {
  "Les Misérables network":         "lesMis",
  "Rome graph":                     "rome",
  "Jazz Network":                   "jazz",
  "Small Directed Network":         "smallDir",
  "Pro League Network":             "proLeague",
  "Argumentation network":          "argumentation",
  "Compact argumentation network":  "argumentationSmaller",
  "Political blogosphere network":  "polBlog",
  "Custom graph":                   "customGraph",
  "Custom directed graph":          "customDir",
  "Custom tree":                    "customTree",
  "Custom graph Force Directed":    "customGraphFD",
  "Custom Multilayered graph":      "customMultiLayeredGraph"
}

def get_path(dataset):
  """
  Returns the relative path of the chosen dataset
  """
  return dataset_devname_to_path[dataset]

def get_devname(realname):
  """
  Returns the developer name of the given dataset name
  """
  return dataset_realname_to_devname[realname]