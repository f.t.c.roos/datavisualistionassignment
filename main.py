import loadingFile  as lf
import drawing      as draw
import treeHelpers  as th
import bonusHelpers as bh
import utils        as ut

# this is the main entree of the program
def cli():
  print("Welcome to our application. We are Floris & Floris.")
  stop = False
  while not stop:
    print()
    steps = [
        "Step 1: Circular Graph Layout",
        "Step 2: Tree Visualization",
        "Step 3: Force-Directed Layout",
        "Step 4: Sugiyama Framework",
        "Step 5: Bonus Tasks",
        "Step 6: Multilayer Graph Layout",
        "Step 7: Projection Layout"
    ]
    print("You can enter at any point '-1' to stop the program.")
    print("You can enter at any point '-2' to start the program again.")
    print()
    print("What visualization step do you want to see?")
    for i in range(len(steps)):
        print(i, ": ", steps[i])

    print()

    userInputStep = None
    print("Please enter a single digit to specify the chosen step.")
    while userInputStep not in range(-2, len(steps)):
        try:
            userInputStep = int(input())
        except:
            print("Only the numbers 0 to", len(steps)-1, "can be given")

    if userInputStep == -1:
      stop = True
      break
    elif userInputStep == -2:
      continue

    print("You chose step:", steps[userInputStep])
    print()

    if userInputStep == 0:
      userInputDataset = None

      datasets = [
        "Les Misérables network",
        "Jazz Network",
        "Rome graph",
        "Small Directed Network",
        "Pro League Network"
      ]

      print("These", len(datasets), "datasets can be used in this step:")
      for i in range(len(datasets)):
        print(i, ": ", datasets[i])

      print()
      print("Please enter a single digit to specify the dataset")
      while userInputDataset not in range(-2, len(datasets)):
          try:
              userInputDataset = int(input())
          except:
              print("Only the numbers 0 to", len(datasets)-1, "can be given")

      if userInputDataset == -1:
        stop = True
        break
      elif userInputDataset == -2:
        continue

      graphDevName = ut.get_devname(datasets[userInputDataset])
      print()
      print("This step will now be performed...")
      drawGraph(graphDevName)

    if userInputStep == 1:
      userInputDataset = None

      datasets = [
        "Les Misérables network",
        "Jazz Network",
        "Rome graph"
      ]

      print("These", len(datasets), "datasets can be used in this step:")
      for i in range(len(datasets)):
        print(i, ": ", datasets[i])

      print()
      print("Please enter a single digit to specify the dataset")
      while userInputDataset not in range(-2, len(datasets)):
          try:
              userInputDataset = int(input())
          except:
              print("Only the numbers 0 to", len(datasets)-1, "can be given")

      if userInputDataset == -1:
        stop = True
        break
      elif userInputDataset == -2:
        continue

      graphDevName = ut.get_devname(datasets[userInputDataset])
      print()

      userInputMethod = None
      print("What method do you want to use:")
      print("1: BFS")
      print("2: DFS")

      print()
      print("Please enter a single digit to specify what method")
      while userInputMethod not in range(-2, 3):
          try:
              userInputMethod = int(input())
          except:
              print("Only the single digits 1 or 2 can be given")

      if userInputMethod == -1:
        stop = True
        break
      elif userInputMethod == -2:
        continue

      userInputStart = None
      print("What root node do you want to initialize:")
      print("1: Node 1")
      print("2: Random Node")
      print()

      while userInputStart not in range(-2, 3):
          try:
              userInputStart = int(input())
          except:
              print("Only the single digits 1 or 2 can be given")

      if userInputStart == -1:
        stop = True
        break
      elif userInputStart == -2:
        continue

      userInputLayout = None
      print("Which layout do you want to use:")
      print("1: Layered")
      print("2: Radial")
      print()

      while userInputLayout not in range(-2, 3):
          try:
              userInputLayout = int(input())
          except:
              print("Only the single digits 1 or 2 can be given")

      if userInputLayout == -1:
        stop = True
        break
      elif userInputLayout == -2:
        continue

      start_node    = 0 if userInputStart == 1 else None
      visualisation = "layered" if userInputLayout == 1 else "radial"
      print("This step will now be performed...")

      if userInputMethod == 1:
        if graphDevName == "lesMis":
          y_size = 10
        elif graphDevName == "jazz":
          y_size = 20
        else:
          y_size = 5

        performAndDrawBFSresult(graphDevName, start_node, visualisation, y_size)

      if userInputMethod == 2:
        performAndDrawDFSresult(graphDevName, start_node, visualisation)

    if userInputStep == 2:
      userInputDataset = None

      datasets = [
        "Les Misérables network",
        "Jazz Network",
        "Rome graph"
      ]

      print("These", len(datasets), "datasets can be used in this step:")
      for i in range(len(datasets)):
        print(i, ": ", datasets[i])

      print()
      print("Please enter a single digit to specify the dataset")
      while userInputDataset not in range(-2, len(datasets)):
          try:
              userInputDataset = int(input())
          except:
              print("Only the numbers 0 to", len(datasets)-1, "can be given")

      if userInputDataset == -1:
        stop = True
        break
      elif userInputDataset == -2:
        continue

      graphDevName = ut.get_devname(datasets[userInputDataset])
      print()

      userInputMethod = None
      print("There are two force-directed layout algorithms possible to use:")
      print("1: Fruchterman Reingold Spring Embedder")
      print("2: Eades Spring Embedder")

      print()
      print("Please enter a single digit to specify which force-directed algorithm you want to use.")
      while userInputMethod not in range(-2, 3):
          try:
              userInputMethod = int(input())
          except:
              print("Only the single digits 1 or 2 can be given")

      print()

      if userInputMethod == -1:
        stop = True
        break
      elif userInputMethod == -2:
        continue

      print("This step will now be performed...")
      if userInputMethod == 1:
          forceDirectedLayoutFruchtermanReingold(graphDevName)
      else:
          forceDirectedLayoutEades(graphDevName)

    if userInputStep == 3:
      userInputDataset = None

      datasets = [
        "Small Directed Network",
        "Pro League Network"
      ]

      print("These", len(datasets), "datasets can be used in this step:")
      for i in range(len(datasets)):
        print(i, ": ", datasets[i])

      print()
      print("Please enter a single digit to specify the dataset")
      while userInputDataset not in range(-2, len(datasets)):
          try:
              userInputDataset = int(input())
          except:
              print("Only the numbers 0 to", len(datasets)-1, "can be given")

      if userInputDataset == -1:
        stop = True
        break
      elif userInputDataset == -2:
        continue

      graphDevName = ut.get_devname(datasets[userInputDataset])
      print()

      userInputHeuristic = None
      print("Do you want to use:")
      print("1: Barycenter Heuristic")
      print("2: Median Heuristic")
      print("3: Combination")

      print()
      print("Please enter a single digit to step to perform")
      while userInputHeuristic not in range(-2, 4):
          try:
              userInputHeuristic = int(input())
          except:
              print("Only the single digits 1 or 2 can be given")
      if userInputHeuristic == -1:
        stop = True
        break
      elif userInputHeuristic == -2:
        continue
      elif userInputHeuristic == 1:
        heurstic = "barycenter"
      elif userInputHeuristic == 2:
        heurstic = "median"
      else:
        heurstic = "both"

      print()
      print("This step will now be performed...")
      minimumFeedbackSet(graphDevName, heurstic, False)

    if userInputStep == 4:
      userInputDataset = None

      datasets = [
        "Les Misérables network",
        "Jazz Network",
        "Rome graph"
      ]

      print("These", len(datasets), "datasets can be used in this step:")
      for i in range(len(datasets)):
        print(i, ": ", datasets[i])

      print()
      print("Please enter a single digit to specify the dataset")
      while userInputDataset not in range(-2, len(datasets)):
          try:
              userInputDataset = int(input())
          except:
              print("Only the numbers 0 to", len(datasets)-1, "can be given")

      if userInputDataset == -1:
        stop = True
        break
      elif userInputDataset == -2:
        continue

      graphDevName = ut.get_devname(datasets[userInputDataset])

      print()

      userInputMethod = None
      print("What method do you want to use:")
      print("1: Breadth-first search")
      print("2: Depth-first search")

      print()
      print("Please enter a single digit to specify what method")
      while userInputMethod not in range(-2, 3):
          try:
              userInputMethod = int(input())
          except:
              print("Only the single digits 1 or 2 can be given")

      if userInputMethod == -1:
        stop = True
        break
      elif userInputMethod == -2:
        continue

      print()

      userInputStart = None
      print("What do you want to initialize as root node:")
      print("1: Node 1")
      print("2: Random Node")
      print()

      while userInputStart not in range(-2,3):
          try:
              userInputStart = int(input())
          except:
              print("Only the single digits 1 or 2 can be given")

      if userInputStart == -1:
        stop = True
        break
      elif userInputStart == -2:
        continue

      print()

      userInputLayout = None
      print("Which layout do you want to use:")
      print("1: Layered")
      print("2: Radial")
      while userInputLayout not in range(-2,3):
          try:
              userInputLayout = int(input())
          except:
              print("Only the single digits 1 or 2 can be given")

      if userInputLayout == -1:
        stop = True
        break
      elif userInputLayout == -2:
        continue

      start_node    = 0 if userInputStart == 1 else None
      visualisation = "layered" if userInputLayout == 1 else "radial"

      print()
      print("This step will now be performed...")

      if userInputMethod == 1:
        if graphDevName == "lesMis":
          y_size = 10
        elif graphDevName == "jazz":
          y_size = 20
        else:
          y_size = 5

        performAndDrawBFSresultBonus(graphDevName, start_node, visualisation, y_size)

      if userInputMethod == 2:
        performAndDrawDFSresultBonus(graphDevName, start_node, visualisation)

    if userInputStep == 5:
      datasets = [
        "Compact argumentation network",
      ]

      print("Only the \"Argumentation Network\" dataset can currently be used in this step.")
      graphDevName = ut.get_devname(datasets[0])
      print("The layout only uses the two subgraphs \"Youngest Devonian Strata\" and \"Gap in the Sequence of Devonshi\".")

      print()
      print("This step will now be performed...")
      multilayerGraphs("argumentationSmaller", "\"Youngest Devonian Strata\"", "\"Gap in the Sequence of Devonshi\"")

    if userInputStep == 6:
      userInputDataset = None

      datasets = [
        "Les Misérables network"
      ]

      print("Only \"Les Misérables network\" dataset can currently be used in this step.")
      graphDevName = ut.get_devname(datasets[0])

      print()

      userInputProjection = None
      print("Do you want to use:")
      print("1: MDS")
      print("2: t-SNE")
      print("Please enter a single digit to step to perform")
      while userInputProjection not in [1, 2]:
          try:
              userInputProjection = int(input())
          except:
              print("Only the single digits 1 or 2 can be given")

      if userInputProjection == -1:
        stop = True
        break
      elif userInputProjection == -2:
        continue

      projection = "MDS" if userInputProjection == 1 else "t-SNE"

      print()
      print("This step will now be performed...")
      graphProjection(graphDevName, projection)

    print("NEXT")
  print("END")

# step 1: circlular layout
def drawGraph(graphName):
    graph = lf.load_graph_from_dataset(graphName)
    draw.drawGraphOfSingleCircle(graph)

# step 2: Draw BFS tree with layered or radial layout
def performAndDrawBFSresult(graphName, start_node, visualisation, y_size=5, circle_rad=0.4):
    graph = lf.load_graph_from_dataset(graphName)
    tree  = th.bfs(graph, start_node)
    tree.root.update_size()

    if visualisation == "layered":
        draw.drawTreeLayered(tree, y_size, circle_rad)
    elif visualisation == "radial":
        draw.drawTreeForRadialLayout(tree)

# step 2: Draw DFS tree with layered or radial layout
def performAndDrawDFSresult(graphName, start_node, visualisation, y_size=5, circle_rad=0.4):
    graph = lf.load_graph_from_dataset(graphName)
    tree  = th.dfs(graph, start_node)
    tree.root.update_size()

    if visualisation == "layered":
        draw.drawTreeLayered(tree, y_size, circle_rad)
    elif visualisation == "radial":
        draw.drawTreeForRadialLayout(tree)

# step 3: Force directed layout by Eades
def forceDirectedLayoutEades(graphName):
    graph = lf.load_graph_from_dataset(graphName)
    draw.EadesPlacementAndDrawing(graph)

# step 3: Force directed layout by Fruchterman and Reingold
def forceDirectedLayoutFruchtermanReingold(graphName):
    graph = lf.load_graph_from_dataset(graphName)
    draw.FruchtermanReingoldPlacementAndDrawing(graph)

# step 4 layered graph layout
def minimumFeedbackSet(graphName, heuristic, showDummy):
    graph = lf.load_graph_from_dataset(graphName)
    draw.drawDirectedGraphLayered(graph, heuristic, showDummy)

# step 5: bonus assignment
def performAndDrawBFSresultBonus(graphName, start_node, visualisation, y_size=5, circle_rad=0.4):
    graph           = lf.load_graph_from_dataset(graphName)
    tree, non_edges = bh.bfs_non_edges(graph, start_node)
    tree.root.update_size()

    if visualisation == "layered":
        draw.drawTreeLayered(tree, y_size, circle_rad, True, non_edges)
    elif visualisation == "radial":
        draw.drawTreeForRadialLayout(tree, True, non_edges)

# step 5: bonus assignment
def performAndDrawDFSresultBonus(graphName, start_node, visualisation, y_size=5, circle_rad=0.4):
    graph           = lf.load_graph_from_dataset(graphName)
    tree, non_edges = bh.dfs_non_edges(graph, start_node)
    tree.root.update_size()

    if visualisation == "layered":
        draw.drawTreeLayered(tree, y_size, circle_rad, True, non_edges)
    elif visualisation == "radial":
        draw.drawTreeForRadialLayout(tree, True, non_edges)

# step 6: multilayered graph with tho subgraphs, without edge bundling.
def multilayerGraphs(graphName, nameSubgraph1, nameSubgraph2):
    multi_layered_graph = lf.load_multilayer_graph_from_dataset(graphName, nameSubgraph1, nameSubgraph2)
    draw.drawMultiLayeredGraph(multi_layered_graph, nameSubgraph1, nameSubgraph2)

# step 7: t-SNE and MDS
def graphProjection(graphName, projection):
    graph = lf.load_graph_from_dataset(graphName)
    draw.drawProjectionGraph(graph, projection)

if __name__ == "__main__":
    cli()