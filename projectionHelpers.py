import numpy              as np
from scipy.sparse.csgraph import shortest_path
from sklearn.manifold     import MDS, TSNE

def createWeightMatrixFromList(graph):
  verticeNumber      = len(graph.adj_list)
  graph.edge_weights = np.zeros((verticeNumber, verticeNumber))

  for edges, weight in graph.adj_edge_label_list.items():
      u = graph.nodes.get(edges[0])
      v = graph.nodes.get(edges[1])

      if weight == []:
        weight = [('weight', '1')]

      graph.edge_weights[u.id, v.id] = weight[0][1]

def createSimilarityMatrix(graph, projection):
  dist_mat                = graph.edge_weights
  dist_mat[dist_mat == 0] = np.max(dist_mat) + 1e09
  np.fill_diagonal(dist_mat, 0)

  if projection == "MDS":
    sim_matrix = shortest_path(dist_mat, return_predecessors=False, method='FW', directed=False, unweighted=False)

    mds    = MDS(n_components=2, dissimilarity='precomputed', normalized_stress="auto", max_iter=1000)
    layout = mds.fit_transform(sim_matrix)

  if projection == "t-SNE":
    tsne   = TSNE(n_components=2, perplexity=30, n_iter=10_000, metric='precomputed', init="random")
    layout = tsne.fit_transform(dist_mat)

  for i, node_name in enumerate(graph.adj_list):
    node = graph.nodes.get(node_name)
    node.x = layout[i, 0]
    node.y = layout[i, 1]

  return layout